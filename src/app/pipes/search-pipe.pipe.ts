import { Pipe, PipeTransform } from "@angular/core";
import { isNullOrUndefined } from "util";
import Person from "../shared/models/person.model";

@Pipe({
  name: "searchPipe"
})
export class SearchPipePipe implements PipeTransform {
  transform(persons: Person[], searchStr: string): any {
    if (!isNullOrUndefined(persons) && searchStr.length > 0) {
      let newArr = persons.filter(person => {
        return (
          person.firstName.toLowerCase().indexOf(searchStr.toLowerCase()) ===
            0 ||
          person.lastName.toLowerCase().indexOf(searchStr.toLowerCase()) === 0
        );
      });
      return newArr;
    } else return persons;
  }
}
