import { Component, OnInit } from '@angular/core';
import Person from '../shared/models/person.model';
import { PostServiceService } from '../shared/services/post-service.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  title = "Контакты";
  searchStr = "";
  persons: Person[] = [];
  constructor(private postsAPI: PostServiceService) { }
  async doGet() {
    try {
      let res =  await this.postsAPI.getPosts();
      console.log('resapp');
      console.log(typeof res);
      console.log(res);
      if (typeof res !== 'undefined') {
        // console.log(res.toString());
        // tslint:disable-next-line: forin
        for (const pers in res) {
          this.persons.push(new Person(res[pers].name, res[pers].surname, res[pers].phone, res[pers].id));
          console.log('---------');
          console.log(res[pers]);
        }
      }
    } catch (e) {
      console.error(e);
    }
  }

  ngOnInit() {
    this.doGet();
  }

  onAddPerson(person) {
    let newId = +this.persons[this.persons.length - 1].id + 1;
    person.id = newId;
    this.persons.push(person);
  }
  onEditPerson(person) {
    this.persons.splice(
      this.persons.findIndex(elem => elem.id == person.id),
      1,
      person
    );
    this.searchStr = "";
  }
  deleteFromArr(id) {
    this.persons.splice(this.persons.findIndex(elem => elem.id == id), 1);
    this.searchStr = "";
  }
}
