import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import Person from "../shared/models/person.model";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpHeaders } from '@angular/common/http';
import { PostServiceService } from '../shared/services/post-service.service';


@Component({
  selector: "app-person-view",
  templateUrl: "./person-view.component.html",
  styleUrls: ["./person-view.component.css"]
})
export class PersonViewComponent implements OnInit {
  @Input() person: Person;
  @Output() delete = new EventEmitter<number>();
  @Output() editPerson = new EventEmitter<Person>();
  updUser: FormGroup;
  nowEdit = false;

  constructor(private postsAPI: PostServiceService) {}
  
  async deleteData(id) {
    try {
      let a = await this.postsAPI.deletePosts(id);
    } catch (error) {
      console.error(error);
    }
  }
  async putData(data) {
    console.log('PUT');
    console.log(data);
    try {
      let a = await this.postsAPI.putPosts(data, data.id);
      return a;
    } catch (e) {
      console.error(e);
    }
  }
  onDelete(id: number) {
    this.deleteData(id);
    this.delete.emit(id);
  }
  onEdit(personId) {
    let phone;
    if (this.nowEdit) {
      if (this.updUser.value.phone[0] == "+") {
        phone = this.updUser.value.phone;
      } else {
        phone = "+7" + this.updUser.value.phone;
      }
      let editedPerson = new Person(
        this.updUser.value.firstName,
        this.updUser.value.lastName,
        phone,
        personId
      );
      let data = {
        id: personId,
        name: this.updUser.value.firstName,
        surname: this.updUser.value.lastName,
        phone
      };
      this.putData(data);
      console.log(data);
      this.editPerson.emit(editedPerson);
    }
    this.nowEdit = !this.nowEdit;
  }
  
  ngOnInit() {
    // console.log(a);
    
    this.updUser = new FormGroup({
      firstName: new FormControl(this.person.firstName, [Validators.required]),
      lastName: new FormControl(this.person.lastName, [Validators.required]),
      phone: new FormControl(this.person.phone, [
        Validators.required,
        Validators.maxLength(7)
      ])
    });
  }
}
