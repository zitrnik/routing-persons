import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Person from '../shared/models/person.model';
import { PostServiceService } from '../shared/services/post-service.service';

@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.css']
})
export class PersonEditComponent implements OnInit {
  id: number;
  updUser: FormGroup;
  person: Person = new Person('', '', '', 0);
  @Output() delete = new EventEmitter<number>();
  @Output() editPerson = new EventEmitter<Person>();
  constructor(public router: Router, public activatedRoute: ActivatedRoute, public postsAPI: PostServiceService) {
    this.activatedRoute.params.subscribe(param => {this.id = param.id;});

  }
  async putData(data) {
    console.log('PUT');
    console.log(data);
    try {
      const a = await this.postsAPI.putPosts(data, data.id);
      return a;
    } catch (e) {
      console.error(e);
    }
  }
  async deleteData(id) {
    try {
      const a = await this.postsAPI.deletePosts(id);
    } catch (error) {
      console.error(error);
    }
  }

  async doGet() {
    try {
      const res =  await this.postsAPI.getPosts();
      // console.log('resapp');
      // console.log(typeof res);
      console.log(res);
      if (typeof res !== 'undefined') {
        // console.log(res.toString());
        // tslint:disable-next-line: forin
        for (const pers in res) {
          if (res[pers].id == this.id) {
            this.updUser.setValue({firstName: res[pers].name, lastName: res[pers].surname, phone: res[pers].phone});
            console.log(this.person);
          } else {
            console.log(res[pers].id);
          }
          console.log('---------');
          console.log();
        }
      }
    } catch (e) {
      console.error(e);
    }
  }
  onDelete(id: number) {
    this.deleteData(id);
    this.delete.emit(id);
    this.router.navigate(['/']);
  }
  onEdit(personId) {
    let phone;
    if (this.updUser.value.phone[0] == '+') {
        phone = this.updUser.value.phone;
      } else {
        phone = '+7' + this.updUser.value.phone;
      }
    let editedPerson = new Person(
        this.updUser.value.firstName,
        this.updUser.value.lastName,
        phone,
        personId
      );
    let data = {
        id: personId,
        name: this.updUser.value.firstName,
        surname: this.updUser.value.lastName,
        phone
      };
    this.putData(data);
    console.log(data);
    this.editPerson.emit(editedPerson);
    this.router.navigate(['/']);
  }
  ngOnInit() {
    this.doGet();
    this.updUser = new FormGroup({
      firstName: new FormControl(this.person.firstName, [Validators.required]),
      lastName: new FormControl(this.person.lastName, [Validators.required]),
      phone: new FormControl(this.person.phone, [
        Validators.required,
        Validators.maxLength(7)
      ])
    });
  }

}
