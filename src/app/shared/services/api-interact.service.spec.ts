import { TestBed } from '@angular/core/testing';

import { ApiInteractService } from './api-interact.service';

describe('ApiInteractService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiInteractService = TestBed.get(ApiInteractService);
    expect(service).toBeTruthy();
  });
});
