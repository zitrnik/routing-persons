import { Injectable } from '@angular/core';
import { ApiInteractService } from './api-interact.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostServiceService extends ApiInteractService {
  url = 'contacts';
  header: HttpHeaders;
  constructor(http: HttpClient) {
    super(http);
    this.header = new HttpHeaders({ 'Content-Type': 'application/json' });
    // this.header.set("Content-type", "application/json");
   }

  async getPosts() {
    return await this.get(this.url, this.header).toPromise();
  }
  postPosts(data) {
    console.log('postpost');
    console.log(data);
    return this.post(this.url, data, this.header).toPromise();
  }
  putPosts(data, id) {
    console.log('putPostService');
    console.log(data);
    return this.put(this.url+'/'+id, data, this.header).toPromise();
  }
  deletePosts(id) {
    return this.delete(this.url+'/'+id, this.header).toPromise();
  }
}
