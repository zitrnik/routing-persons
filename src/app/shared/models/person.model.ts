export default class Person {
  toLowerCase() {
    throw new Error("Method not implemented.");
  }
  public id: number;
  public firstName: string;
  public lastName: string;
  public phone: string;
  constructor(
    firstName: string,
    lastName: string,
    phone?: string,
    id?: number
  ) {
    (this.id = id),
      (this.firstName = firstName),
      (this.phone = phone),
      (this.lastName = lastName);
  }
}
